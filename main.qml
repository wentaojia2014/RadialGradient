import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
Item {
    width: 640
    height: 480
    Row{
        spacing: 5
        Image {
            id: imageSource
            source: "s.jpg"
        }
        //从图像中心，一圈一圈地显现出来。即透明度圆形渐变。
        ShaderEffect {
            id: shader
            width: imageSource.width
            height: imageSource.height
            visible: true
            property var source: imageSource
            property vector2d size: Qt.vector2d(imageSource.width, imageSource.height)
            //percent表示百分比，即当前显示出来的图像占全部图像的比例，用动画来控制百分比
            property real percent: 0.0
            NumberAnimation on percent {
                from: 0
                to: 1
                duration: 4000
                loops: -1
            }
            fragmentShader: "
varying highp vec2 qt_TexCoord0;
uniform highp float percent;
uniform sampler2D source;
void main() {
    float distance = length(qt_TexCoord0 - vec2(0.5, 0.5));
    vec4 color = texture2D(source, qt_TexCoord0);
//    float alpha = step(distance, percent);
//    float alpha = smoothstep(0.0, percent, distance);
    highp float alpha = 0.0;
    if (distance < 0.0 || distance > percent)
        alpha = 0.0;
    else
        alpha = percent;
    gl_FragColor = vec4(color.rgb, alpha);
}"
        }
    }
}
